const descripcion = {
		demand: true,//indica si el comando es obligatorio
		alias: 'd',
		desc: 'Descripcion de la tarea por hacer'
};
const completado = {
	defautl: true,//por defecto
	alias: 'c',
	desc: 'Marco como completado o pendiente la tarea'		

}
//Definir los comandos

const argv = require('yargs')
	.command('crear', 'Crear un elemnto por hacer', {
		descripcion
	})
	.command('actualizar', 'Actulizar el estado completado de una tarea', {
		descripcion,
		completado
	})
	.command('borrar', 'Borrar un elemnto de la lista', {
		descripcion
	})

	.help()
	.argv;

module.exports = {
	argv
}
