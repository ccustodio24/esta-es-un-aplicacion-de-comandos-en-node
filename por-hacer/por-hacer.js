const fs = require('fs');

let listadoPorHacer = [];
const guardarDB =  () => {
	let data = JSON.stringify(listadoPorHacer);//combierte un objeto a formato json
	fs.writeFile('db/data.json', data, (err) => {
		if (err) throw new Error('No se pudo grabar ', err);
	});
}

const cargarDB = () => {
	try {
		listadoPorHacer = require('../db/data.json');
	} catch (error) {
		listadoPorHacer = [];//Esta validaciones porque cuando o hay nada en archivo json manda un error
	}
}
const getListado = () => {
	cargarDB();
	return listadoPorHacer;
}
const crear = (descripcion) => {
	cargarDB();
	let porHacer = {
		descripcion,
		completado: false
	};
	listadoPorHacer.push(porHacer);
	guardarDB()
	return porHacer;
}
const actualizar = (descripcion, completado = true) => {
	cargarDB();
	//findIndex : recorre todo el arreglo y me devulve el index de cada item, si no existe me devuelve -1
	let index = listadoPorHacer.findIndex(tarea => {
		return tarea.descripcion === descripcion;
	})
	if(index >= 0){
		listadoPorHacer[index].completado = completado;
		guardarDB();
		return true;
	}else{
		return false;
	}
}
const borrar = (descripcion) => {
	cargarDB();
	//findIndex : recorre todo el arreglo y me devulve el index de cada item, si no existe me devuelve -1
	let nuevoListado = listadoPorHacer.filter(tarea => {
		return tarea.descripcion !=  descripcion;
	})	
	if (listadoPorHacer.length === nuevoListado.length) {
		return false;
	} else {
		listadoPorHacer = nuevoListado;
		guardarDB();
		return true;
	}
}
module.exports = {
	crear,
	getListado,
	actualizar,
	borrar
}